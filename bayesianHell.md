
Bayesian Hell: primo e secondo girolo
========================================================
author:  Mateusz Łącki
date: 8 April 2015      

========================================================
Simple Example

- $X_1, \dots, X_n | \mu, \lambda \sim N(\mu , \frac{1}{\lambda})$ 
- $\mu \sim N(\gamma, \frac{1}{\delta})$
- $\lambda \sim Gamma(\alpha, \beta)$

It translates into:

$f_{X_1, \dots, X_n}(x_1, \dots, x_n| \lambda, \mu) = C \times \prod_{i = 1}^n \lambda^{1/2} \exp\Big( - \frac{\lambda (x_i - \mu)^2}{2}\Big)$

$f(\mu ) = D \times \exp\Big( - \frac{ \delta (\mu - \gamma)^2}{2} \Big)$

$f(\lambda) = E \times \lambda^{\alpha - 1} \exp( - \beta \lambda)$

========================================================
Given parameters $\gamma, \delta, \alpha, \beta$ it is easy to sample the space. 


```r
gamma <- 2; delta <- 4; alpha <- 2; beta <- 1
xs <- replicate(
    n = 25,
    {
        lambda <- rgamma(1, 
            shape = alpha, 
            scale = 1/beta
        )
        mu <- rnorm(1, 
            mean = gamma, 
            sd = sqrt(delta)
        )
        
        rnorm( 100, mean = mu, sd = sqrt(lambda) )
    }
)
```

Slide With Plot
========================================================

![plot of chunk unnamed-chunk-2](bayesianHell-figure/unnamed-chunk-2-1.png) 

Bayesians and unnormalised densities
========================================================
$P(T = t | p) = (1-p)^{t-1} p$
- a good prior on $p$ is $Beta(\alpha, \beta)$: $g(p) = \frac{p^{\alpha-1}(1-p)^{\beta - 1}}{B(\alpha, \beta)}$
- the posterior: f( a | b) = f( b | a) f(a) / f(b). The nominator equals

$(1-p)^{t-1} p \frac{p^{\alpha-1}(1-p)^{\beta - 1}}{B(\alpha, \beta)} \propto (1-p)^{t-1 + \alpha - 1} p^{\beta + 1 - 1}$
- This is again similar to $Beta(\alpha + t - 1, \beta + 1)$.
- Because of $\int f( a | b) d a = 1$ ...
- ... normalising constant = $B(\alpha + t - 1, \beta + 1)$
- In the Gibbs algorithm tracing forms of conditional distributions is crutial.

Time analysis continued 1
========================================================
- Say that initially we knew only that $p \in [0,1]$.
- Take $\alpha = \beta = 1$

```r
a_priori <- data.frame(
    x = x1 <- seq(0,1, length.out = 1000),
    y = dbeta(x1 , 1, 1),
    tag = 'a_priori'
)
```
- Then we observe $T = 5$

```r
a_posteriori <- data.frame(
    x = x1,
    y = dbeta(x1 , 1 + 5 - 1, 1 + 1),
    tag = 'a_posteriori'
)
```

Time analysis continued 2
========================================================

```r
library(dplyr); library(ggplot2)

bind_rows( a_priori, a_posteriori ) %>% 
    mutate( tag = factor(tag) ) %>%
    ggplot(
        aes( x = x, y = y, colour = tag )    
    ) + 
    geom_line()
```

Time analysis continued 3
========================================================
- Data ($T= 5$) updated the distribution of $p$

- There were no analitical problems: the posterior was easily obtainable
    - prior was conjugate to the observation's distribution
    
***
![plot of chunk unnamed-chunk-6](bayesianHell-figure/unnamed-chunk-6-1.png) 


Time analysis continued 4
========================================================
- What if we observed more? 
    - Sample $= T = (T_1, \dots, T_n) = (t_1, \dots, t_n)$
    - Assume independence
        - E.g. independent observations on survival times 

- Likelihood:
$P( T_1 = t_1, \dots, T_n = t_n | p) = \prod_{i = 1}^n (1 - p)^{t_i - 1} p =$
$= (1-p)^{\sum_{i = 1}^n t_i - n} p^n$
- Prior: $Beta(\alpha, \beta)$.
- Posterior: $Beta(\alpha + \sum_{i = 1}^n t_i - n, \beta + n)$.

Time analysis continued 5
======================================================== 

```r
T <- c( 1.1, 2.23, 2.224, 3.1341, 2,352, 2.543, 4.64 )
sumT <- sum(T)
n <- length(n)
```

- $Beta$ mean $= \frac{\alpha}{\alpha+ \beta} =$

```r
alpha <- 1+sumT-n
beta  <- 1+n    

alpha/(alpha + beta)
```

```
[1] 0.9946218
```
    
***
![plot of chunk unnamed-chunk-9](bayesianHell-figure/unnamed-chunk-9-1.png) 
- Variance $= \frac{\alpha\beta}{(\alpha + \beta)^2(\alpha + \beta + 1)}$

```
[1] 1.442356e-05
```


Easy problem: conclusions
========================================================
- The normalising constants are not important **for conjugate families**
- Data updates our initial guess about the parameters
- The output is still a distribution
    - probabilistic questions: mean, devation, modes

What about the initial example?


Back to the initial problem
========================================================
Given the data $X_1 = x_1, \dots, X_n = x_n$, how to infer the parameters?
- The likelihood times the priors on the parameters:

$f_{X_1,\dots,X_n}(x_1,\dots,x_n| \mu, \lambda ) \times f(\mu ) \times f(\lambda) \propto$

$\lambda^{n/2 + \alpha - 1 } \exp\Big( - \lambda/2 \sum_{i=1}^{100} (x_i - \mu)^2 - \frac{\delta}{2} (\mu - \gamma)^2 - \beta \lambda )\Big) =$

$\lambda^{n/2 + \alpha - 1 } \exp\Big( - \lambda(\frac{n v_n}{2} + \frac{n}{2}(\mu - \bar{x})^2 + \beta) - \frac{\delta}{2} (\mu - \gamma)^2 \Big)$

where $\bar{x} = \sum x_i / n$, $v_n = \sum (x_i - \bar{x})^2/n$


========================================================
- Two parameters need an update: $\lambda, \mu$
- Their joint distribution conditional on data $x = (x_1, \dots, x_n)$:

$$ f( \lambda, \mu| x) \propto f(x | \lambda, \mu) \times f(\lambda) \times f(\mu) $$

- No reason why $f(x | \lambda, \mu)= g(\lambda) h(\mu)$
    - posterior parameters are no longer independent

<img src="figures/ourCollider.png" alt="Drawing" style="width: 300px;"/>

Bayesian nets graph representation 1
========================================================
$$f(x_1, x_2, x_3) =$$ 
$$f(x_1) f(x_2 | x_1) f(x_3 | x_1, x_2)=$$
- **chain** 
$$ = f(x_1) f(x_2 | x_1) f(x_3| x_2)$$
- **collider**
$$ = f(x_1) f(x_2 )f(x_3 | x_1, x_2)$$
- **fork**
$$ = f(x_1) f(x_2 | x_1) f(x_3 | x_1)$$

***
<img src="figures/chain.png" alt="Drawing" style="width: 400px;"/>
<img src="figures/collider.png" alt="Drawing" style="width: 220px;"/>
<img src="figures/fork.png" alt="Drawing" style="width: 250px;"/>

Bayesian nets graph representation 2
========================================================
<img src="figures/chain.png" alt="Drawing" style="width: 400px;"/>
- **Chain**: given $X_2$, $X_1$ and $X_3$ are conditionally independent

<img src="figures/collider.png" alt="Drawing" style="width: 220px;"/>
- **Collider**: given $X_1$ are $X_2$ are independent, unless $X_3$ is observed.

Bayesian nets graph representation 3
========================================================
<img src="figures/fork.png" alt="Drawing" style="width: 250px;"/>
- **Fork**: given $X_1$, $X_2$ and $X_3$ are conditionally independent

- Perks of graphs:
    - help performing calculations
    - help expressing modelling ideas
        - modelling dependency pathways

Old big problem with Bayesian Analysis
========================================================
- We want to simulate a multivariate variable $\theta = (\lambda, \mu)$. 
- It's difficult: $\pi(\lambda, \mu) = f(\lambda, \mu|x) =$
$\lambda^{n/2 + \alpha - 1 } \exp\Big( - \lambda(\frac{n v_n}{2} + \frac{n}{2}(\mu - \bar{x})^2 + \beta) - \frac{\delta}{2} (\mu - \gamma)^2 \Big)$
    - density is a function of both $\lambda$ and $\mu$
- Markov Chain Monte Carlo comes to the rescue
    - start with some initial distribution of $\theta_0 = (\lambda_0, \mu_0)$
        - e.g. one-point distribution $P(\lambda_0 = 4, \mu_0 = 4) = 1$
    - walk around the **state-space** $[0, \infty) \times \mathbb{R} \ni (\lambda, \mu)$ in a special way


Slide a little bit influenced by the Sting
========================================================
- Exploring the *shape of my distribution*
- Exploration
    - *every step you take* is bloody stochastic $\theta_0, \theta_1, \theta_2, \dots$
    - every step's distribution depends just on the previous step (markovness) - it's a chain: de do do do, de da da da.
<img src="figures/mcmc.png" alt="Drawing" style="width: 500px;"/>
    - the collection of randomly drawn points $\{ \theta_t \}_{t = 0, 1, \dots}$ starts to behave like $\pi$
        - e.g. if $B$ is a circle, then $\int_B \pi \approx \frac{\text{Simulated points inside B}}{\text{Simulated points}}$
    - Roxanne doesn't have to put on the red light. 

Gibbs, glorious Gibbs
========================================================
- Start with any point $\theta_0 = (\theta_{01}, \dots, \theta_{0d} )$
- $d$ - dimensionality of the posterior distribution
- Update $\theta_{01}$ by $\theta_{11} \sim \pi( \theta_1 | \theta_{02}, \dots, \theta_{0d}) \propto \pi( \theta_1, \theta_{02}, \dots, \theta_{0d})$
- Update $\theta_{02}$ by $\theta_{21} \sim \pi( \theta_2 | \theta_{11}, \theta_{03}, \dots, \theta_{0d}) \propto \pi( \theta_{11}, \theta_{2}, \theta_{03}, \dots, \theta_{0d})$
- and so on so on, cyclically.

- Such algorithm fullfils our requirements (it's proved).
- Can we use it to solve our initial problem?
    - If not why would I mention it? Of course it bloody can...

========================================================
$\pi \propto \lambda^{n/2 + \alpha - 1 } \exp\Big( - \lambda(\frac{n v_n}{2} + \frac{n}{2}(\mu - \bar{x})^2 + \beta) - \frac{\delta}{2} (\mu - \gamma)^2 \Big)$
- $f(\lambda | \mu, x) = \frac{f(\lambda, \mu)|x}{f(\mu | x)} \propto f(\lambda, \mu | x)$

$f(\lambda | \mu, x) \propto \lambda^{n/2 + \alpha - 1 } \exp( - \lambda(\frac{n v_n}{2} + \frac{n}{2}(\mu - \bar{x})^2 + \beta))$
- So $\lambda|\mu, x \sim Gamma( n/2 + \alpha, \frac{n v_n}{2} + \frac{n}{2}(\mu - \bar{x})^2 + \beta)$

$f(\mu| \lambda, x) \propto f(\lambda, \mu | x) \propto \exp( - \frac{\lambda n}{2}(\mu - \bar{x})^2- \frac{\delta}{2} (\mu - \gamma)^2 )$ 

$\lambda n (\mu - \bar{x})^2 + \delta (\mu - \gamma)^2 = (\lambda n+ \delta)(\mu - \frac{\lambda n \bar{x} + \gamma \delta}{\lambda n + \delta})^2 + \frac{(\gamma - \bar{x})^2}{\frac{1}{n\lambda} + \frac{1}{\delta}}$

- So $\mu|x,\lambda \sim N(\frac{\lambda n \bar{x} + \gamma \delta}{\lambda n + \delta}, \frac{1}{\lambda n+ \delta})$

The Gibbs algorithm
========================================================

1. Start with any $\mu^{[0]}, \lambda^{[0]}$ and data $x$.
2. Calculate empirical moments $\bar{x}, v_n$.
3. Repeat until bloody death or for 50K iterations:
    - $\lambda^{[n+1]} \sim Gamma( n/2 + \alpha, \frac{n v_n}{2} + \frac{n}{2}(\mu^{[n]} - \bar{x})^2 + \beta)$   
    - $\mu^{[n+1]} \sim N(\frac{\lambda^{[n+1]} n \bar{x} + \gamma \delta}{\lambda^{[n+1]} n + \delta}, \frac{1}{\lambda^{[n+1]} n+ \delta})$
4. After cutting the burn-in initial 5K iterations, consider $\{ (\lambda^{[n]}, \mu^{[n]}) : n = 5K + 1, \dots, 50K\}$ to be a good discrete approximation of $f(\lambda, \mu | x)$.  

Implementation 1
========================================================


```r
x <- xs[,24] # A possible realisation of the sample.

xbar <- mean(x); vn <- var(x); n <- length(x)

sampling <- list(
lambda = function(mu) rgamma(
    n = 1,
    shape = (n/2 + alpha), 
    scale = 1/(n*vn/2+n*(mu-xbar)^2/2+beta)
),
mu = function(lambda) rnorm(
    n = 1,
    mean = (lambda*n*xbar+gamma*delta)/(lambda*n+delta),
    sd = 1/sqrt(lambda*n + delta)
)
)
```

Implementation 2
========================================================

```r
results <- matrix(ncol = 50000, nrow = 2)
mu <- 4; lambda <- 4 #Initial guess

for( i in 1:50000 ){
    lambda  <- sampling$lambda( mu )
    mu      <- sampling$mu( lambda )
    results[,i] <- c(lambda, mu)
}

results <- results[,5001:50000]

results <- data.frame(t(results))
names(results) <- c('lambda', 'mu')
```


Visualisation 1
========================================================

```r
proxyDistrPlot <- ggplot(
    data = results,
    aes(
        x = lambda,
        y = mu,
        alpha = 0.1
    )
) + geom_point()

plot(proxyDistrPlot)
```

Visualisation 2
========================================================

- Thanks to the ergodic theory we know that in the limit the distributions match.
- But the finite approximation is not error-free.


***
![plot of chunk unnamed-chunk-14](bayesianHell-figure/unnamed-chunk-14-1.png) 

Marginal posterior distributions summary
========================================================

```r
sapply(results, summary)
```

```
        lambda       mu
Min.     3.964 -0.35540
1st Qu.  4.704 -0.18880
Median   4.861 -0.15870
Mean     4.866 -0.15870
3rd Qu.  5.025 -0.12850
Max.     5.933  0.03575
```

```r
sapply(results, sd)
```

```
   lambda        mu 
0.2384558 0.0451874 
```

Into real life problems
========================================================
- Let's look at some diabetes data


```r
# install.packages('monomvn'); install.packages('lars'); install.packages('dplyr')

library(monomvn); library(lars)
data(diabetes)
colnames(diabetes$x)
```

```
 [1] "age" "sex" "bmi" "map" "tc"  "ldl" "hdl" "tch" "ltg" "glu"
```
- Ten baseline explanatory variables $X$ for $n=442$ patients.

Real life problem 1
========================================================
- Response $y$: a quantitative measure of disease progression one year after baseline

```r
summary(diabetes$y)
```

```
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
   25.0    87.0   140.5   152.1   211.5   346.0 
```

Real life solutions 
========================================================
Assume linear model
$$y = \mu 1 + X\beta + \epsilon$$
- $y = (y_1, \dots, y_{442})$
- $p = 10$ parameters $\beta = (\beta_1, \dots, \beta_{10})$ and a mean (+1)
- $\epsilon \sim N(0, \sigma^2 \mathbb{I}_{442\times442})$
- Under these conditions alone we can already estimate the parameters $\beta$ using the least squares


```r
linearModel <- lm( diabetes$y ~ diabetes$x )    
```

Ordinary least squares estimates
========================================================

```

Call:
lm(formula = diabetes$y ~ diabetes$x)

Residuals:
     Min       1Q   Median       3Q      Max 
-155.829  -38.534   -0.227   37.806  151.355 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)    152.133      2.576  59.061  < 2e-16 ***
diabetes$xage  -10.012     59.749  -0.168 0.867000    
diabetes$xsex -239.819     61.222  -3.917 0.000104 ***
diabetes$xbmi  519.840     66.534   7.813 4.30e-14 ***
diabetes$xmap  324.390     65.422   4.958 1.02e-06 ***
diabetes$xtc  -792.184    416.684  -1.901 0.057947 .  
diabetes$xldl  476.746    339.035   1.406 0.160389    
diabetes$xhdl  101.045    212.533   0.475 0.634721    
diabetes$xtch  177.064    161.476   1.097 0.273456    
diabetes$xltg  751.279    171.902   4.370 1.56e-05 ***
diabetes$xglu   67.625     65.984   1.025 0.305998    
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Residual standard error: 54.15 on 431 degrees of freedom
Multiple R-squared:  0.5177,	Adjusted R-squared:  0.5066 
F-statistic: 46.27 on 10 and 431 DF,  p-value: < 2.2e-16
```

Potential problems
========================================================
- It can be proved that the least square estimate of $\beta$ is $$\hat{\beta} = (X^t X)^{-1} X^t y$$
- and that $\hat{\beta} \sim N\Big(\beta, \sigma^2  (X^t X)^{-1} \Big)$
- That is highly problematic in case when there are more descriptive variable than observations, $n < p$.
    - Like too many gene expressions measured per patient...
    - $X^t X$ is singular and cannot be inversed
- Solution: assume that not all of the $p$ explanatory variable play a role in the problem
    - punish the model for having to many non-zero $\beta$.

Lasso solution
========================================================
- Lasso (probably invented in Texas) assumes that the estimate $\hat{\beta}_{lasso}$ should minimise

$$||y-X\beta||^2_2 + \lambda \sum_{i=1}^p |\beta_i|$$
for some $\lambda$ - shrinkage parameter.

- It can be proved that such estimate always exist.
- Can be efficiently computed using the Least Angle Regression algorithm (Efron, Hastie, Johnstone, Tibshirani 2004).


Bayesian Lasso 1
========================================================
Assume linear model
$$y = \mu 1 + X\beta + \epsilon$$

- $\epsilon \sim N(0, \sigma^2 \mathbb{I}_{442\times442})$
- $\beta_1, \dots, \beta_p \sim Laplace(\frac{\lambda}{\sigma})$
    - $Laplace(a)$ density is: $g(\beta) \propto e^{ -a|\beta| }$
- A priori: $\beta$ is almost zero
    - $X$ has no influence upon the response.
 

***
![plot of chunk unnamed-chunk-20](bayesianHell-figure/unnamed-chunk-20-1.png) 
<img src="figures/blasso.png" alt="Drawing" style="width: 200px;"/>

Bayesian Lasso 2
========================================================
- Park and Casella (2008) showed analytical form of the conditional parameters $\theta | \theta_{-i}$ 
- One can perform Gibbs on that problem.
    - It's implemented (with some modifications...)
    

```r
reg.blas <- blasso(diabetes$x, diabetes$y)

plot(reg.blas, burnin = 200)
```

========================================================

```
t=100, m=8
t=200, m=7
t=300, m=8
t=400, m=7
t=500, m=7
t=600, m=8
t=700, m=6
t=800, m=9
t=900, m=7
```

![plot of chunk unnamed-chunk-22](bayesianHell-figure/unnamed-chunk-22-1.png) 
